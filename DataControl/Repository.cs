﻿using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.Sql;

using MySql.Data.Entity;

using Data;

namespace DataControl {

	//[DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
	public class RepContext:DbContext {
		public RepContext(){
			this.Database.EnsureCreated();
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder){
      		optionsBuilder.UseMySql("server=127.0.0.1;database=LearnToLearn;uid=root;password=password;");
    	}

		protected override void OnModelCreating(ModelBuilder mb){
			mb.Entity<User>()
    				.Property(b => b.Id)
    				.ValueGeneratedOnAdd();
			mb.Entity<Course>()
    				.Property(b => b.Id)
    				.ValueGeneratedOnAdd();
			mb.Entity<Role>()
					.Property(x=>x.Id)
					.ValueGeneratedOnAdd();
			mb.Entity<Enrollment>()
    				.Property(b => b.Id)
    				.ValueGeneratedOnAdd();
			mb.Entity<Grade>()
    				.Property(b => b.Id)
    				.ValueGeneratedOnAdd();

			mb.Entity<User>().HasMany<Enrollment>(x=>x.Enrollments).WithOne().OnDelete(DeleteBehavior.Cascade);
			mb.Entity<User>().HasMany<Course>(x=>x.Courses).WithOne().OnDelete(DeleteBehavior.Cascade);
			mb.Entity<Course>().HasMany<Enrollment>(x=>x.Enrollments).WithOne().OnDelete(DeleteBehavior.Cascade);
			mb.Entity<Enrollment>().HasOne<Grade>(x=>x.UserGrade).
									WithOne(x=>x.Parent).
									HasForeignKey<Grade>(x=>x.ParentId).
									OnDelete(DeleteBehavior.Cascade);
		}

		public virtual DbSet<User> Users { get; set; }
	}

	public class Repository<T> where T:Base,new(){
		public RepContext context;
		public DbSet<T> CurrentSet;

		public Repository() {
			if (context == null) {
				context = new RepContext();
				
				foreach(var usr in context.Users){
					context.Entry(usr).Collection(x=>x.Courses).Load();
					//context.Entry(usr).Collection(x=>x.Enrollments).Load();

					foreach(var enr in usr.Courses){
						//context.Entry(enr).Reference(x=>x.Course).Load();
						context.Entry(enr).Collection(x=>x.Enrollments).Load();
					}
					
					foreach(var enr in usr.Courses){
						foreach(var crs in enr.Enrollments){
							context.Entry(crs).Reference(x=>x.UserGrade).Load();
						}
					}

					context.Entry(usr).Reference(x=>x.UserRole).Load();
				}

			}

			if(context.Users.Count()==0){
				context.Users.Add(new User{
                    Name="John DOe",
					Email="sdasdasd",
                    Phone="00889877999",
					Password="1111111",
					UserRole=new Role{
						Id=1,
						IsAdmin=true,
						IsTeacher=false
					},
					Courses=new List<Course>{
							new Course{
							Name="Initial COurse",
							Description="None",
							Capacity=10,
							Id=1,
							IsVisible=true
						}
					}
                });
				context.SaveChanges();
			}

			T item = new T();
			if (item is User) {
				CurrentSet = context.Users as DbSet<T>;
			}
		}
		
		public void Insert(T item){
			if (Contains(item)) {
				return;
			}

			CurrentSet.Add(item);

			context.SaveChanges();
		}

		public void Delete(T item){
			if(!Contains(item)){
				return;	
			}
			item = Get(item);

			context.Entry(item).State=EntityState.Deleted;
			context.SaveChanges();
		}
	
		public void Update(int id,T item){
			var elem = CurrentSet.Find(id);

			if(elem==null){
				return;	
			}
			item.Id = (elem as Base).Id;
			context.Entry(elem).CurrentValues.SetValues(item);

			context.SaveChanges();
		}

		public bool Contains(T item){
			foreach (var elem in CurrentSet) {
				if(item.Equals(elem)){
					return true;
				}
			}
			return false;
		}

		public int GetId(T item){
			foreach (var elem in CurrentSet) {
				if(item.Equals(elem)){
					var temp = elem as Base;
					return temp.Id;
				}
			}
			return -1;
		}

		public T GetById(int item){
			foreach (var elem in CurrentSet) {
				if ((elem as Base).Id==item) {
					return elem as T;
				}
			}
			return null;
		}

		public T Get(T item){
			foreach (var elem in CurrentSet) {
				if (item.Equals(elem)) {
					return elem as T;
				}
			}
			return null;
		}

		public List<T> Get(){
			return CurrentSet.ToList();
		}
	}
}
