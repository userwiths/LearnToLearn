using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.Sql;

using MySql.Data.Entity;

using Data;

namespace DataControl
{
    public class Service
    {
        public Repository<User> rep{get;set;}

        public Service(){
            rep=new Repository<User>();
        }

        #region USER
        public void AddUser(User item){
            rep.CurrentSet.Add(item);
            rep.context.SaveChanges();
        }
        public void RemoveUser(int id){
            rep.CurrentSet.Remove(rep.context.Find<User>(id));
            rep.context.SaveChanges();
        }
        public void UpdateUser(int id,User usr){
            rep.context.Entry(rep.context.Find<User>(id)).CurrentValues.SetValues(usr);
            rep.context.SaveChanges();
        }
        public User GetUser(int id){
            return rep.CurrentSet.Where(x=>x.Id==id).FirstOrDefault();
        }
        public List<User> GetAllUsers(){
            return rep.context.Users.ToList();
        }
        #endregion

        #region COURSE
        public void AddCourse(int usrId,Course cr){
            var elem=rep.context.Users.Where(x=>x.Id==usrId).FirstOrDefault();
            elem.Courses.Add(cr);
            rep.context.Entry(cr).State=EntityState.Added;
            rep.context.SaveChanges();
        }
        public void RemoveCourse(int id){
            foreach(var usr in rep.context.Users){
                if(usr.Courses.Where(x=>x.Id==id).Count()!=0){
                    usr.Courses.Remove(usr.Courses.Where(x=>x.Id==id).First());
                }
            }
            rep.context.Entry(rep.context.Find<Course>(id)).State=EntityState.Deleted;
            rep.context.SaveChanges();
        }
        public void UpdateCourse(int id,Course cr){
            rep.context.Entry(rep.context.Find<Course>(id)).State=EntityState.Modified;
            rep.context.Entry(rep.context.Find<Course>(id)).CurrentValues.SetValues(cr);
            rep.context.SaveChanges();
        }
        public Course GetCourse(int id){
            return rep.context.Find<Course>(id);
        }
        public List<Course> GetAllCourses(){
            List<Course> result=new List<Course>();
            foreach(var usr in  rep.CurrentSet){
                result.AddRange(usr.Courses);
            }
            return result;
        }
        #endregion

        #region ENROLLMENT
        public void AddEnrollment(int usrId,int courseId,Enrollment cr){
            var elem=rep.context.Users.Where(x=>x.Id==usrId).FirstOrDefault();
            var course=rep.context.Find<Course>(courseId);

            course.Enrollments.Add(cr);
            elem.Enrollments.Add(cr);
            
            rep.context.Entry(cr).State=EntityState.Added;
            rep.context.SaveChanges();
        }
        public void RemoveEnrollment(int id){
            foreach(var usr in rep.context.Users){
                if(usr.Enrollments.Where(x=>x.Id==id).Count()!=0){
                    usr.Enrollments.Remove(usr.Enrollments.Where(x=>x.Id==id).First());
                }
            }
            rep.context.Entry(rep.context.Find<Enrollment>(id)).State=EntityState.Deleted;
            rep.context.SaveChanges();
        }
        public void UpdateEnrollment(int id,Enrollment cr){
            rep.context.Entry(rep.context.Find<Enrollment>(id)).State=EntityState.Modified;
            rep.context.Entry(rep.context.Find<Enrollment>(id)).CurrentValues.SetValues(cr);
            rep.context.SaveChanges();
        }
        public Enrollment GetEnrollment(int id){
            return rep.context.Find<Enrollment>(id);
        }
        public List<Enrollment> GetAllEnrollments(){
            List<Enrollment> result=new List<Enrollment>();
            foreach(var usr in  rep.CurrentSet){
                result.AddRange(usr.Enrollments);
            }
            return result;
        }
        #endregion
    
        #region GRADE
        public void SetGrade(int usrId,int courseId){
            
        }
        #endregion
    }
}