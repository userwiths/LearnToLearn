using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Formatting;
using System.Net.Http;
using System.Web;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;

using System.Security;
using System.Security.Cryptography;
using System.Security.Authentication;
using System.Security.Claims;
using Microsoft.IdentityModel;
using System.Text;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.Extensions.Options;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.Sql;

using MySql.Data.Entity;

using Data;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EnrollmentController:Controller
    {
        public static DataControl.Repository<User> rep;

        private void Check(){
            if(rep==null){
                rep=new DataControl.Repository<User>();
            }
        }

        //[Authorize(Roles="Teacher")]
        [HttpGet]
        public List<Enrollment> Get(){
            Check();
            int a=User.Claims.Where(x=>x.Type==ClaimTypes.Name).Select(x=>int.Parse(x.Value)).First();
            return rep.CurrentSet.Where(x=>x.Id==a).First().Enrollments;
        } 

        //[Authorize(Roles="Teacher")]
        [HttpGet("{id}")]
        public Enrollment Get(int id){
            Check();
            int a=User.Claims.Where(x=>x.Type==ClaimTypes.Name).Select(x=>int.Parse(x.Value)).First();
            return rep.CurrentSet.Where(x=>x.Id==a).First().Enrollments.Where(y=>y.Id==id).First();
        }

        //[Authorize(Roles="Teacher")]
        [HttpPost("{id}")]
        public void Add(int id,[FromBody]Enrollment usr){
            Check();
            int a=int.Parse(User.Claims.Where(x=>x.Type==ClaimTypes.Name).First().Value);
            var user=rep.CurrentSet.Where(x=>x.Id==a).First();

            rep.context.Users.Where(x=>x.Courses.Count!=0 &&
                                    x.Courses.Where(y=>y.Id==id).Count()!=0).
                                    First().
                                    Courses.
                                    Where(x=>x.Id==id).
                                    First().
                                    Enrollments.
                                    Add(usr);
            usr.UserGrade.Parent=usr;
            usr.UserGrade.ParentId=usr.Id;
            user.Enrollments.Add(usr);
            
            rep.context.Entry(usr).State=EntityState.Added;
            rep.context.Entry(usr.UserGrade).State=EntityState.Added;
            
            rep.context.SaveChanges();
        }

        //[Authorize(Roles="Teacher")]
        [HttpDelete("{id}")]
        public void Remove(int id){
            Check();
            int a=User.Claims.Where(x=>x.Type==ClaimTypes.Name).Select(x=>int.Parse(x.Value)).First();
            var user=rep.CurrentSet.Where(x=>x.Id==a).First();
            var del=user.Enrollments.Where(x=>x.Id==id).First();
            //user.Enrollments.Remove(del);

            foreach(var usr in rep.context.Users){
                foreach(var cr in usr.Courses){
                    if(cr.Enrollments.Contains(del)){
                        cr.Enrollments.Remove(del);
                    }
                }
            }
            rep.context.Entry(del).State=EntityState.Detached;
            rep.context.Entry(del).State=EntityState.Deleted;
            rep.context.SaveChanges();
        }

        [Authorize(Roles="Teacher")]
        [HttpPut("{id}")]
        public void Update(int id,[FromBody] Enrollment nw){
            Check();
            int a=User.Claims.Where(x=>x.Type==ClaimTypes.Name).Select(x=>int.Parse(x.Value)).First();
            var user=rep.CurrentSet.Where(x=>x.Id==a).First();
            var del=user.Enrollments.Where(x=>x.Id==id).First();
            
            rep.context.Entry(del).CurrentValues.SetValues(nw);
            rep.context.Entry(del).State=EntityState.Modified;
            rep.context.SaveChanges();
        }
        
    }
}