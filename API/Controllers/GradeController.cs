using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Formatting;
using System.Net.Http;
using System.Web;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;

using System.Security;
using System.Security.Cryptography;
using System.Security.Authentication;
using System.Security.Claims;
using Microsoft.IdentityModel;
using System.Text;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.Extensions.Options;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.Sql;

using MySql.Data.Entity;

using Data;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GradeController:Controller
    {
        public static DataControl.Repository<User> rep;

        private void Check(){
            if(rep==null){
                rep=new DataControl.Repository<User>();
            }
        }

        //[Authorize(Roles="Teacher")]
        [HttpGet("{id}")]
        public Grade Get(int id){
            Check();
            int a=User.Claims.Where(x=>x.Type==ClaimTypes.Name).Select(x=>int.Parse(x.Value)).First();
            return rep.CurrentSet.Where(x=>x.Id==a).First().Enrollments.Where(y=>y.Id==id).First().UserGrade;
        }

        [Authorize(Roles="Teacher")]
        [HttpPut("{id}")]
        public void Update(int id,[FromBody] Grade nw){
            Check();
            int a=User.Claims.Where(x=>x.Type==ClaimTypes.Name).Select(x=>int.Parse(x.Value)).First();
            var grade=rep.CurrentSet.Where(x=>x.Id==a).First().Enrollments.Where(y=>y.Id==id).First().UserGrade;
            
            rep.context.Entry(grade).CurrentValues.SetValues(nw);
            rep.context.Entry(grade).State=EntityState.Modified;
            rep.context.SaveChanges();
        }
        
    }
}