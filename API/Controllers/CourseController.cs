using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Formatting;
using System.Net.Http;
using System.Web;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;

using System.Security;
using System.Security.Cryptography;
using System.Security.Authentication;
using System.Security.Claims;
using Microsoft.IdentityModel;
using System.Text;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.Extensions.Options;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.Sql;

using MySql.Data.Entity;

using Data;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController:Controller
    {
        public static DataControl.Repository<User> rep;

        private void Check(){
            if(rep==null){
                rep=new DataControl.Repository<User>();
            }
        }

        //[Authorize(Roles="Teacher")]
        [HttpGet]
        public List<Course> Get(){
            Check();
            int a=User.Claims.Where(x=>x.Type==ClaimTypes.Name).Select(x=>int.Parse(x.Value)).First();
            return rep.CurrentSet.Where(x=>x.Id==a).First().Courses;
        } 

        //[Authorize(Roles="Teacher")]
        [HttpGet("{id}")]
        public Course Get(int id){
            Check();
            int a=User.Claims.Where(x=>x.Type==ClaimTypes.Name).Select(x=>int.Parse(x.Value)).First();
            return rep.CurrentSet.Where(x=>x.Id==a).First().Courses.Where(y=>y.Id==id).First();
        }

        //[Authorize(Roles="Teacher")]
        [HttpPost]
        public void Add([FromBody]Course usr){
            Check();
            int a=User.Claims.Where(x=>x.Type==ClaimTypes.Name).Select(x=>int.Parse(x.Value)).First();
            var user=rep.CurrentSet.Where(x=>x.Id==a).First();
            
            user.Courses.Add(usr);
            rep.context.Entry(usr).State=EntityState.Added;

            rep.context.SaveChanges();
        }

        //[Authorize(Roles="Teacher")]
        [HttpDelete("{id}")]
        public void Remove(int id){
            Check();
            int a=User.Claims.Where(x=>x.Type==ClaimTypes.Name).Select(x=>int.Parse(x.Value)).First();
            var user=rep.CurrentSet.Where(x=>x.Id==a).First();
            var del=user.Courses.Where(x=>x.Id==id).First();
            user.Courses.Remove(del);

            rep.context.Entry(del).State=EntityState.Detached;
            rep.context.Entry(del).State=EntityState.Deleted;
            rep.context.SaveChanges();
        }

        [Authorize(Roles="Teacher")]
        [HttpPut("{id}")]
        public void Update(int id,[FromBody] Course nw){
            Check();
            int a=User.Claims.Where(x=>x.Type==ClaimTypes.Name).Select(x=>int.Parse(x.Value)).First();
            var user=rep.CurrentSet.Where(x=>x.Id==a).First();
            var del=user.Courses.Where(x=>x.Id==id).First();
            
            rep.context.Entry(del).CurrentValues.SetValues(nw);
            rep.context.Entry(del).State=EntityState.Modified;
            rep.context.SaveChanges();
        }
        
    }
}