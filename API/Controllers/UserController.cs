using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Formatting;

using System.Security;
using System.Security.Cryptography;
using System.Security.Authentication;
using System.Security.Claims;
using Microsoft.IdentityModel;
using System.Text;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Web;

using Microsoft.AspNetCore.Http;

using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;

using Data;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController
    {
        public static DataControl.Repository<User> rep;

        private void Check(){
            if(rep==null){
                rep=new DataControl.Repository<User>();
            }
        }

        [Authorize]
        [HttpGet]
        public List<User> Get(){
            Check();
            return rep.context.Users.ToList();
        } 

        [Authorize(Roles="Admin")]
        [HttpGet("{id}")]
        public User GetById(int id){
            Check();
            return rep.GetById(id);
        }

        [Authorize(Roles="Admin")]
        [HttpPost()]
        public void Add([FromBody]User usr){
            Check();
            rep.Insert(usr);
        }

        
        [Authorize(Roles="Admin")]
        [HttpDelete("{id}")]
        public void Remove(int id){
            Check();
            rep.Delete(rep.GetById(id));
        }

        [Authorize(Roles="Admin")]
        [HttpPut("{id}")]
        public void Update(int id,[FromBody] User nw){
            Check();
            rep.Update(id,nw);
        }
        
    }
}