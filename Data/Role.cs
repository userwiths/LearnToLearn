using System;
using System.ComponentModel;

namespace Data
{
    public class Role:Base
    {
        public bool IsAdmin{get;set;}
        public bool IsTeacher{get;set;}

        public Role(){
            IsAdmin=false;
            IsTeacher=false;
        }

        public override bool Equals(object obj){
            Role r=obj as Role;
            if(r==null){
                return false;
            }
            return r.IsAdmin==IsAdmin && r.IsTeacher==IsTeacher;
        }
    }
}