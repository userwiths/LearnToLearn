using System;

namespace Data
{
    public class Enrollment:Base{
        private Grade usergrade;
        //public Course Course{get;set;}
        public Grade UserGrade{
            get=>usergrade;
            set{usergrade=value;UpdateTime();}
        }
        public DateTime CreatedOn{get;set;}
        public DateTime UpdatedOn{get;set;}

        private void UpdateTime(){
            UpdatedOn=DateTime.Now;
        }

        public Enrollment(){
        //    Course=new Course();
            UserGrade=new Grade();
            CreatedOn=DateTime.Now;
            UpdatedOn=DateTime.Now;
        }
    }
}