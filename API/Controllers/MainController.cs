using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Formatting;

using System.Security;
using System.Security.Cryptography;
using System.Security.Authentication;
using System.Security.Claims;
using Microsoft.IdentityModel;
using System.Text;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Web;

using Microsoft.AspNetCore.Http;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.Extensions.Options;

using System.Web.Http;

using Data;
using DataControl;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MainController
    {

        //[Authorize(Roles="Admin")]
        [HttpPost]
        public string Get([FromBody] Models.LogInModel log){
            Repository<User> usr=new Repository<User>();

            User result=usr.context.Users.Where(x=>x.Name==log.Name && x.Password==log.Password).First();

            if (result!=null){
                var claims = new Claim[]{
                    new Claim(ClaimTypes.Name,result.Id.ToString()),
                    new Claim(ClaimTypes.Role,result.UserRole.IsAdmin?"Admin":result.UserRole.IsTeacher?"Teacher":"Student"),
                    new Claim(ClaimTypes.Email,result.Email)
                };

            string key="Qwery_2_Qwerty_3_Qwerty_4_Qwerty_5_Qwerty";

            var secKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var creds = new SigningCredentials(secKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: "http://localhost:5000",
                audience: "http://localhost:5000",
                claims: claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: creds);

                return  new JwtSecurityTokenHandler().WriteToken(token);
            }

            return "Could not verify username and password";
        }

        /*public User GetUser(this Controller cntr){
            var rep=new Repository<User>();
            int a=cntr.User.Claims.Where(x=>x.Type==ClaimTypes.Name).Select(x=>int.Parse(x.Value)).First();
            var user=rep.CurrentSet.Where(x=>x.Id==a).First();
            return user;
        } */
    }
}