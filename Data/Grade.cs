//using System.ComponentModel.Design;
using System;

namespace Data
{
  //  [ComplexType]
    public class Grade:Base
    {
        public int ParentId{get;set;}
        public Enrollment Parent{get;set;}
        private double score_number;
        
        public double Score{
            get=>score_number;
            set=>score_number=value%5+2;
        }

        public char Mark{
            get{
                char result='\0';
                switch(Math.Abs(score_number)){
                    case 2:result='F';break;
                    case 3:result='D';break;
                    case 4:result='C';break;
                    case 5:result='B';break;
                    case 6:result='A';break;
                    default:result='N';break;
                }
                return result;
            }
        }

        public string Comment{get;set;}

        public Grade(){
            Score=6;
            Comment="None";

        }
    }
}