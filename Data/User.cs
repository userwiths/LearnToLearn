﻿using System;
using System.Collections.Generic;

namespace Data
{
    public class User:Base{
        public string Name{get;set;}
        public string Password{get;set;}
        public string Email{get;set;}
        public Role UserRole{get;set;}

        public List<Course> Courses{get;set;}

        public List<Enrollment> Enrollments{get;set;}
        public string Phone{get;set;}

        public User(){
            Courses=new List<Course>();
            Enrollments=new List<Enrollment>();
            UserRole=new Role();

        }
    }
}
