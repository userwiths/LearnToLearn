using System;
using System.Collections.Generic;

namespace Data
{
    public class Course:Base{
        private string name;
        private string description;
        private int capacity;
        private bool isvisible;
        private DateTime created;
        private DateTime updated;

        public string Name{
            get=>name;
            set {name=value;UpdateTime();}
        }
        public string Description{
            get=>description;
            set {description=value;UpdateTime();}
        }
        public int Capacity{
            get=>capacity;
            set{capacity=value;UpdateTime();}
        }
        public bool IsVisible{
            get=>isvisible;
            set{isvisible=value;UpdateTime();}
        }
        public DateTime CreatedOn{
            get=>created;
            //set{created=value;UpdateTime();}
        }
        public DateTime UpdatedOn{get=>updated;}

        public Course(){
            Enrollments=new List<Enrollment>();
            Name="None";
            Description="None";
            Capacity=0;
            IsVisible=false;
            created=DateTime.Now;
            updated=DateTime.Now;
        }

        private void UpdateTime(){
            this.updated=DateTime.Now;
        }

        public List<Enrollment> Enrollments{get;set;}

        public override bool Equals(object obj){
            Course c=obj as Course;
            if(c==null){
                return false;
            } 
            return 
                c.Name==Name &&
                c.Capacity==Capacity && 
               // c.UserId==UserId && 
                c.CreatedOn==CreatedOn && 
                c.Description==Description;
        }
    }
}